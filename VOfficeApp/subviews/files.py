from django.http import HttpResponse, JsonResponse
import json
from django.http import FileResponse



from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from VOfficeApp.submodels.files import FileUpload, UploadFolder, FileShareUsers, createFileEntry
from VOfficeApp.submodels.company import *
from VOfficeApp.models import User


class DownloadFileView(APIView):
   
    authentication_classes = []#[authentication.TokenAuthentication]
    permission_classes = []#[IsAuthenticated]
    
    def get(self, request, file_id):
        file = FileUpload.objects.get(id=file_id)
        
        #if not checkFileAccess(request, file):            
        #    return HttpResponse(status=403)
            
        
        return FileResponse(open(file.file.path, 'rb'))

def uploadFile(request):
    
    createFileEntry(request)
   
    return HttpResponse(status=200)

def shareFileUser(request):
    
    """
    file_id = request.FILES['file']
    target_user_id = request.POST['target']
    
    file = FileUpload.objects.get(id=file_id)
    target_user = User.objects.get(id=target_user_id)
    
    if file.shared_users is None:
        file.shared_users = FileShareUsers.objects.create() 
        file.save()
    
    file.shared_users.add(target_user);
    file.shared_users.save()    
   """
    return HttpResponse(status=200)

def shareFileLevel(request):

    jsonData = json.loads(request.POST['data'])
   
    file_id = jsonData['file']    
    level_id = jsonData['level']    
    level_type = jsonData['type']    
    
    file = FileUpload.objects.get(id=file_id)
    
    if file.shared_users is None:
        file.shared_users = FileShareUsers.objects.create() 
        file.save()        
    
    if level_type == 'department':
        level = Department.objects.get(id=level_id)
        file.shared_users.departments.add(level)
        
    elif level_type == 'branch':
        level = Department.objects.get(id=level_id)
        file.shared_users.branches.add(level)
    
    
    return HttpResponse(status=200)

def addFolder(request):
    
    jsonData = json.loads(request.POST['data'])
   
    path = jsonData['path']    
    
    folder = UploadFolder.objects.create(path=path, owner=request.user) 
    folder.save()       
       
    return HttpResponse(status=200)

def listFolders(request):
    
    answer = {'folders': []}
       
        
    for folder in request.user.uploadfolder_set.all():
        answer['folders'].append(folder.path)
  
    return JsonResponse(answer)

def listFiles(request):
    
    answer = {'files': []}
       
        
    for f in request.user.fileupload_set.all():
        
        path = ""
        if f.folder is not None:
            path = f.folder.path
            
        answer['files'].append({
            'id': f.id,
            'name': f.name,
            'path': path
        })
  
    return JsonResponse(answer)

def listSharedFiles(request):
    
    answer = {'files': []}
       
    
    shares = FileShareUsers.objects.filter(users=request.user.id)
        
    for share in shares.all():
        
        path = ""
        if share.fileupload.folder is not None:
            path = share.fileupload.folder.folder.path
            
        answer['files'].append({
            'id': share.fileupload.id,
            'name': share.fileupload.name,
            'path': path
        })
  
    return JsonResponse(answer)

  
def checkFileAccess(request, file):

    if file.owner == request.user:
        return True;
    
    if file.shared_users is not None:
            if file.shared_users.users.filter(id=request.user.id).exists():
                return True
            
            #company = request.user.userdata.employee_data.assignment.company            
            #if company is None:                
            
            department = request.user.userdata.employee_data.assignment.department
            
            if department is not None:
                if file.shared_users.departments.filter(id=department.id).exists():
                    return True
            
            
    if file.folder is not None:
        if file.shared_users.users.filter(id=request.user.id).exists():
                return True;
                
def downloadFile(request, file_id):
    
    file = FileUpload.objects.get(id=file_id)
    
    if not checkFileAccess(request, file):
        
        return HttpResponse(status=403)
        
    
    return FileResponse(open(file.file.path, 'rb'))