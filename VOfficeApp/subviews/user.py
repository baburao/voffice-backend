#from django.shortcuts import render

from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.http import FileResponse

from django.views.decorators.http import require_http_methods
#from django.contrib.auth.models import Permission
from django.contrib.auth.decorators import login_required
import json

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token

from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from VOfficeApp.models import User,Designation, user_type_from_string
from VOfficeApp.submodels.userdata import EmployeeData, ClientData, GuestData, VendorData, ProfileImage,\
    Notification
from VOfficeApp.submodels.employee import EmployeeInformation
from VOfficeApp.submodels.room import Room
from VOfficeApp.submodels.company import Company, GroupCompany, Branch, Department, EmployeeAssignment, Team
from VOfficeApp.subviews.company import createDepartment
from VOfficeApp.room_handler import RoomHandler
from django.core.mail import send_mail

from VOfficeApp.subviews.management import assignUserToLevel

import random
import string


from VOfficeApp.lib.company_db import execute_function
from VOfficeApp.lib import company_functions
from VOfficeApp.lib.logger import *


class UserLoginView(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
                
        token, created = Token.objects.get_or_create(user=user)
        
        answer = getUserData(user)
        screen_permissions = getUserPermissions(user.id,request.data.get('domain'))
        answer['token'] = token.key
        answer['email'] = user.email
        answer['screen_permissions'] = screen_permissions
        return Response(answer)
       
class UserLogoutView(APIView):
    
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        RoomHandler.instance.unregisterUser(request.user.id)
        request.user.auth_token.delete()
        
        return Response()
 
class UserChangePasswordView(ObtainAuthToken):

    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request, *args, **kwargs):
        
        password = request.data.get('password')                
        
        request.user.set_password(password)
        request.user.save()
         
        return Response()

class ResetPasswordView(ObtainAuthToken):

    authentication_classes = []
    permission_classes = []
    
    def post(self, request, *args, **kwargs):
        
        send_mail(
            'TEST',
            'teststestststes',
            'test@voffis.com',
            ['steffem2000@gmail.com'],
            fail_silently=False,
        )
         
        return Response()
     
#helper function to create a new user
def creatUser(user_type, email, first_name, last_name, designation_id, password='test'):
    
    designation = Designation.objects.get(id=designation_id)
    user = User.objects.create_user(email=email, first_name=first_name, last_name=last_name, password=password, username='user', designation=designation)
    user_data = user.userdata
         
    if user_type=='employee':
        user_data.employee_data = EmployeeData.objects.create()
        user_data.employee_data.employee_information = EmployeeInformation.objects.create()
        user_data.employee_data.employee_information.save()
        user_data.employee_data.save()
    elif user_type=='guest':
        user_data.guest_data = GuestData.objects.create()
    elif user_type=='client':
        user_data.client_data = ClientData.objects.create()
    elif user_type=='vendor':
        user_data.vendor_data = VendorData.objects.create()
    else:
        return None
    
    user_data.save()    
    user.save()
    
    return user

class AddEmployee(ObtainAuthToken):

    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request, *args, **kwargs):
            
   
        email = request.data.get('email')
        first_name = request.data.get('firstname')
        last_name = request.data.get('lastname')
        user_type = 'employee'
        designation = request.data.get('designation')
        
        if len(email) <= 0:        
            return Response(status=500)  
    
        if not creatUser(user_type, email, first_name, last_name, designation) is None:
            return Response(status=200) 
        
        return Response(status=500) 


def findFreeTable(desk_tables, max_workspaces):
    
    current_table = 0
    
    while desk_tables[current_table] is not None and len(desk_tables[current_table]) < max_workspaces:
        current_table += 1
        
    return current_table

def findFreeSlot(workspace_set, max_workspaces):
    for s in range(max_workspaces):
        if not s in workspace_set:
            return s
        
    return -1

        
#@login_required()


#@login_required()        
#@require_http_methods(["Get"])   
def listNotAssignEmployees(request):
    
    answer = {        
        'users': []
    }
    
    for u in User.objects.filter(Q(user_type='e'), Q(userdata__employee_data__assignment__isnull=True)):        
        #if u.is_superuser == False:
        answer['users'].append({
            'id':u.id,
            'email': u.email
        })  
    
    return JsonResponse(answer)

#helper function to add a user entry to a mapping
def user_to_json(user):
    return {
        'id': user.id,
        #'email': user.email,
        'firstname': user.first_name,
        'lastname': user.last_name,
        'designation': user.designation_id,
        'is_online': RoomHandler.instance.isConnected(user.id)
    }
def addUserData(user , user_map):
    
    #profile_image_id = -1
    #if user.userdata.profile_image is not None:
    #    profile_image_id = user.userdata.profile_image_id
        
    user_map[user.id]= user_to_json(user)
    
def getUserData(user):
    
    profile_image_id = -1
    if user.userdata.profile_image is not None:
        profile_image_id = user.userdata.profile_image_id

    return {
        'id': user.id,
        #'email': user.email,
        'firstname': user.first_name,
        'lastname': user.last_name,
        #'profileImage' : profile_image_id,
        'designation': user.designation_id,
        'is_online': True
    }

def getUserPermissions(user_id, domain):

    execute_function.get_connection_by_company(0)

    params = {'ip_domain': domain}
    company_id = execute_function.get(params, company_functions.fn_get_company_id_by_domain)
    if company_id > 0:
        execute_function().close_db_connection()
        execute_function.get_connection_by_company(company_id)

    params = {'ip_user_id': user_id}
    screen_permissions = execute_function.get(params, company_functions.fn_get_user_permissions)
    log_result(screen_permissions)

    return screen_permissions

class GetAdminView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        admin  = {
            'name': 'AccountAdmin',
            'users': 'Role'
        }

        return Response(admin)



class GetUserView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        answer = getUserData(request.user)

        return Response(answer)
    
class ListUsersView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request, user_type_param):
        
        userMap = {}
        
        user_type = user_type_from_string(user_type_param.lower())
    
        for u in User.objects.filter(user_type=user_type):   
            #TODO ??
            if u.id != request.user.id:
                if not u.is_superuser:
                    addUserData(u , userMap)
                
        answer = { 
            'user':  getUserData(request.user) ,
            'users' : userMap
        }
        
        return Response(answer)
    
#@login_required()
#@require_http_methods(["Get"])  
def listUsers(request, user_type_param):
    
    answer = {        
        'users': {}
    }
    user_type = user_type_from_string(user_type_param.lower())
    
    for u in User.objects.filter(user_type=user_type):    
        addUserData(u ,  answer['users'])
    
    return JsonResponse(answer)

#@login_required()
#@require_http_methods(["Get"])  
def getUserInformation(request, user_id):
    
    user = User.objects.get(id=user_id)
    info = user.userdata.employee_data.employee_information
        
    answer = {        
        'phone': info.contact_number,
        #'bio': info.bio,
    }
    
    return JsonResponse(answer)

#@login_required()
@require_http_methods(["POST"])   
def setUserInformation(request):
    jsonData = json.loads(request.POST['data'])
   
    user_id = jsonData['user']
    phone = jsonData['phone']
    bio = jsonData['bio']  
    
    user = User.objects.get(id=user_id)
    info = user.userdata.employee_data.employee_information
    
    info.contact_number = phone
    info.bio = bio
    
    info.save()
    
    return HttpResponse(status=200) 

    
class GetAllNotificationsView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        
        try:
            notification_list = []
            unread_notification = Notification.objects.filter(user=request.user) 
            
            for notification in unread_notification.all():
                notification.has_read = True
                notification.save()
                notification_list.append(notification.to_json())
            
            return Response(notification_list) 
        except:
            #print("eeee")
            return Response({})


class GetNotificationsView(APIView):
   
    # authentication_classes = [authentication.TokenAuthentication]
    # permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        
        try:
            notification_list = []
            unread_notification = Notification.objects.filter(user=request.user, has_read=False) 
            
            for notification in unread_notification.all():
                #notification.has_read = True
                #notification.save()
                notification_list.append(notification.to_json())
            
            return Response(notification_list) 
        except:
            #print("eeee")
            return Response({})
        
class ReadNotificationsView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        
        notification_id = request.data.get('id')        
        notification = Notification.objects.get(id=notification_id)
        notification.has_read = True
        notification.save()
                
        return Response()

class ProfileImageView(APIView):
   
    authentication_classes = []#[authentication.TokenAuthentication]
    permission_classes = []#[IsAuthenticated]
    
    def get(self, request, image_size, user_id):
        
        
        try:
            user = User.objects.get(id=user_id)
            
            image_file = ''
            
            if user.userdata.profile_image is None:
                if image_size == 'small':
                    image_file = 'default_avatar_small.png'
                elif image_size == 'big':
                    image_file = 'default_avatar.png'
            
            else:
                if image_size == 'small':
                    image_file = user.userdata.profile_image.small_image.path
                elif image_size == 'big':
                    image_file = user.userdata.profile_image.big_image.path 
            
            return FileResponse(open(image_file, 'rb'))         
        except:
            if image_size == 'small':
                image_file = 'default_avatar_small.png'
            elif image_size == 'big':
                image_file = 'default_avatar.png'
            #print("eeee")
            return FileResponse(open(image_file, 'rb'))       
        
class UploadProfileImageView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        upload_file = request.FILES['file']
        
        
        if request.user.userdata.profile_image is not None:
            request.user.userdata.profile_image.delete()
            request.user.userdata.profile_image = None   
   
        
        request.user.userdata.profile_image = ProfileImage.objects.create(original_image=upload_file)
        
        request.user.userdata.profile_image.save()
        request.user.userdata.save()
   
        return Response()
    
def getProfileImage(request, id):
    
    file = ProfileImage.objects.get(id=id)
    
    return FileResponse(open(file.small_image.path, 'rb'))


def uploadProfileImage(request):
    
    upload_file = request.FILES['file']
    user_id = request.POST['user']
        
    user = User.objects.get(id=user_id)
    
    if user.userdata.profile_image is not None:
        user.userdata.profile_image.delete()
        user.userdata.profile_image = None   
   
        
    user.userdata.profile_image = ProfileImage.objects.create(original_image=upload_file)
    
    user.userdata.profile_image.save()
    user.userdata.save()
   
    return HttpResponse(status=200)
 
 
@require_http_methods(["POST"])   
def createTestRooms(request):
    for i in range(10):
        name = 'Meeting Room %i' % (i + 1)
        size = ((i % 4) + 1) * 4
        
        new_room = Room.objects.create(name=name, size=size, room_type=Room.MEETING_ROOM)
        new_room.save()
        
    return HttpResponse(status=200) 


        
class UserParser():
        
    def __init__(self):
        
        self.sep = '\t'
        self.branch_mapping = {}    
        self.team_mapping = {}    
        self.id_mapping = {}    
        
        
        self.sizeX = 16
        self.sizeY = 30
        self.room_map = []
        
        self.write_clean = False
        
        for i in range(self.sizeX):
            self.room_map.append([0] * self.sizeY)
         
        data_file = None
        
        if self.write_clean: 
            data_file = open('../EmployeeSheet.tsv' , 'rt')
        else:
            data_file = open('EmployeeSheetClean.tsv' , 'rt')
        
        self.clean()
        
        #self.write_clean = True;
        
        self.root_company = Company.objects.create(name='Creativeland Asia')
        self.root_company.save()
        
        self.designation = None
        if Designation.objects.count() == 0:
            self.designation = Designation.objects.create(name="Employee", description="Employee")
            self.designation.save()
        else:    
            self.designation = Designation.objects.first()
            
        for i in range(4):
            name = 'Meeting Room %i' % (i + 1)
            size = ((i % 4) + 1) * 4
            
            new_room = Room.objects.create(name=name, size=size, room_type=Room.MEETING_ROOM)
            new_room.save()
        
        #Top line
        l0 = data_file.readline()
        types = data_file.readline()
        field_names_line = data_file.readline()
        field_names = field_names_line.split(self.sep)
        
        #Examples
        #l1 = data_file.readline()
        #l2 = data_file.readline()        
        #Empty Line
        #l3 =  data_file.readline()  
        
        if self.write_clean:
            
            f = open('EmployeeSheetClean.tsv', "w")
            
            sep = ''
            f.write(l0 + sep + types + sep + field_names_line + sep )
            
            f.close()
            
        self.firstname_index = field_names.index('First Name')
        self.lastname_index = field_names.index('Last Name')   
        self.department_index = field_names.index('Department')   
        self.brands_index = field_names.index('Brands Name')   
        self.status_index = field_names.index('Employee Status(Full/Intern/Consultant/Contract)')   
        self.branch_index = field_names.index('Branch')   
        self.email_index = field_names.index('Email Id')   
        
        line = data_file.readline()
        count = 0
        max_count = -1
        while line and (count < max_count or max_count == -1):            
            
            self.parseUser(line)
            line = data_file.readline()
            count += 1
    
        print("Import users succesfull")
        
        self.createDevUser("steffen")
        self.createDevUser("srikant")
        self.createDevUser("amol")
        self.createDevUser("shani")
        self.createDevUser("abhishek")
        self.createDevUser("test1")
        self.createDevUser("test2")
        self.createDevUser("test3")
    
        for b in self.branch_mapping:
            for d in self.branch_mapping[b]['departments']:
                room = Room.objects.get(id=self.branch_mapping[b]['departments'][d].room.id)
                self.placeRoom(room)
                
    def clean(self):
        
        for u in User.objects.all():
            if not u.is_superuser:
                u.delete()
                
        for c in Company.objects.all():
            c.delete()
        for b in Branch.objects.all():
            b.delete()
        for d in Department.objects.all():
            d.delete()
        for r in Room.objects.all():
            r.delete()
        for t in Team.objects.all():
            t.delete()
                 
    def getBranch(self, branch_name):
        
        if not branch_name in self.branch_mapping:
            branch = Branch.objects.create(name=branch_name, company=self.root_company) 
            branch.save()
            self.branch_mapping[branch_name] = {'branch' : branch, 'departments' : {}}
            
        return self.branch_mapping[branch_name]
    
    def getDepartment(self, branch_name, department_name):
        
        branch = self.getBranch(branch_name)        
        
        if not department_name in self.branch_mapping[branch_name]['departments']:
            
            department = createDepartment(department_name, branch['branch'])
            self.branch_mapping[branch_name]['departments'][department_name] = department
            
        return self.branch_mapping[branch_name]['departments'][department_name]
    
    def getTeam(self, team_name):
        
        if not team_name in self.team_mapping:
            
            team = Team.objects.create(name=team_name)
            self.team_mapping[team_name] = team
        
        return self.team_mapping[team_name]
               
    def parseUser(self, line):
        
        values = line.split(self.sep)
        
        firstname = values[self.firstname_index]
        lastname = values[self.lastname_index]
        department = values[self.department_index]
        brands = values[self.brands_index]
        status = values[self.status_index]
        branch = values[self.branch_index]
        email = values[self.email_index]
        
        
        
        if self.write_clean:
            for i in range(len(values)):            
                if i != self.firstname_index and i != self.email_index and i != self.lastname_index and i != self.department_index and  i != self.brands_index and i != self.status_index and  i != self.branch_index:
                    values[i] = ''
        
            clean_line = '\t'.join(values)
            f = open('EmployeeSheetClean.tsv', "a")
            f.write(clean_line + '\r')
            f.close()
        
        user_type = 'employee'
        
        if len(department) <= 0 or len(branch) <= 0:
            print('Invalid Data:')
            print('%s %s' % (firstname, lastname))
            
            return
        
        #email = firstname + lastname + '@cl.com'
        if email in self.id_mapping:
            print('Name Error:')
            print('%s %s' % (firstname, lastname))
            
            return
        
        self.id_mapping[email] = 1
        
        department = self.getDepartment(branch.strip(), department.strip())
        
        new_user = creatUser(user_type, email, firstname, lastname, self.designation.id)
        
        assignUserToLevel(new_user.id, 'department', department.id)
        
        '''
        if len(brands) > 0:
            teams = brands.split(',')
            
            for team_name in teams:
                
                if len(team_name.strip()) > 0:
                    team = self.getTeam(team_name.strip())
                    if team.creator is None:
                        team.creator=new_user
                    else:
                        team.members.add(new_user)
                    team.save()
        '''
        print('Add user: %s' % email)
        #print(branch)
    
    def createDevUser(self, name):
        user_type = 'employee'
        designation = None
        if Designation.objects.count() == 0:
            designation = Designation.objects.create(name="Test", description="Test")
            designation.save()
        else:    
            designation = Designation.objects.first()
        
        department = self.getDepartment("VOffice", "VOffice Devs")        
        
        new_user = creatUser(user_type, name + '@dev.com', name, "", designation.id)
        
        assignUserToLevel(new_user.id, 'department', department.id)
        
    
    def placeRoom(self, room):
        
        x, y = self.findRoomPlace(room)
        
        room.x = x
        room.y = y
        room.save()
        self.addRoomToMap(room)
        
    def addRoomToMap(self, room):        
        
        if room.x + room.width < self.sizeX and room.y + room.height < self.sizeY:
            for x in range(room.width):
                for y in range(room.height):      
                                                 
                    self.room_map[room.x + x][room.y + y] = 1
                    
        
        #for t in self.room_map:
            #print(t)
            
    def checkPosition(self,x, y, width, height):   
           
        if x + width >= self.sizeX or y + height >= self.sizeY:
            
            return False
        
        for xx in range(width):
            for yy in range(height):    
                if self.room_map[x + xx][y + yy] != 0: 
                                
                    return False
        return True
    
    def findRoomPlace(self, room): 
        
        for y in range(self.sizeY):  
            for x in range(self.sizeX): 
                if self.checkPosition(x, y, room.width, room.height):
                    return x, y
        
        return 0, 0

@require_http_methods(["POST"])   
def loadCompanyData(request):
        
    parser = UserParser()
    
    return HttpResponse(status=200) 

from django.core.files import File

@require_http_methods(["POST"])   
def setProfilePictures(request):
        
    data_file = open('VOffice ProfilePics/Email & Pictures.csv' , 'rt')
    
    l0 = data_file.readline()
    line = data_file.readline()
        
    while line:
        line = data_file.readline()
        data = line.strip().split(',')
        
        if len(data) <= 1:
            continue
        
        email=data[0]
        file=data[1]
        
        try:
            user = User.objects.get(email=email)
            
            if user.userdata.profile_image is not None:
                request.user.userdata.profile_image.delete()
                request.user.userdata.profile_image = None   
            
            #try:
            user.userdata.profile_image = ProfileImage.objects.create(original_image=None)
            
            #user.userdata.profile_image.original_image.save(file, File(open('VOffice ProfilePics/CLA Employee 350x350_Pictures/%s' % file)))
            user.userdata.profile_image.original_image.save(file, File(open('VOffice ProfilePics\\CLA Employee 350x350_Pictures\\%s' % file, 'rb')))
            
            user.userdata.profile_image.save()
            user.userdata.save()
        
        except:
            print("not found: %s " % (data[0]))
                
            
        
    return HttpResponse(status=200) 

#@login_required()
@require_http_methods(["POST"])   
def createTestUsers(request):
    
    user_type = 'employee'
    
    designation = None
    if Designation.objects.count() == 0:
        designation = Designation.objects.create(name="Test", description="Test")
        designation.save()
    else:    
        designation = Designation.objects.first()
    
    for i in range(10):        
        email = 'test%i@test.com' % i
        
        
        first_name = test_firstnames[random.randrange(len(test_firstnames))] #random.choice(string.ascii_uppercase)
        last_name = test_lastnames[random.randrange(len(test_lastnames))] #random.choice(string.ascii_uppercase)
    
        creatUser(user_type, email, first_name, last_name, designation.id)
        
    
    return HttpResponse(status=200) 


test_firstnames = ['Abigail','Alexandra','Alison','Amanda','Amelia','Amy','Andrea','Angela','Anna','Anne','Audrey','Ava','Bella','Bernadette','Carol','Caroline','Carolyn','Chloe','Claire','Deirdre','Diana','Diane','Donna','Dorothy',
'Elizabeth','Ella','Emily','Emma','Faith','Felicity','Fiona','Gabrielle','Grace','Hannah','Heather','Irene','Jan','Jane','Jasmine','Jennifer','Jessica','Joan','Joanne','Julia','Karen','Katherine','Kimberly','Kylie',
'Lauren','Leah','Lillian','Lily','Lisa','Madeleine','Maria','Mary','Megan','Melanie','Michelle','Molly','Natalie','Nicola','Olivia','Penelope','Pippa','Rachel','Rebecca','Rose','Ruth','Sally','Samantha','Sarah','Sonia',
'Sophie','Stephanie','Sue','Theresa','Tracey','Una','Vanessa','Victoria','Virginia','Wanda','Wendy','Yvonne','Zoe','Adam','Adrian','Alan','Alexander','Andrew','Anthony','Austin','Benjamin','Blake','Boris','Brandon',
'Brian','Cameron','Carl','Charles','Christian','Christopher','Colin','Connor','Dan','David','Dominic','Dylan','Edward','Eric','Evan','Frank','Gavin','Gordon','Harry','Ian','Isaac','Jack','Jacob','Jake','James','Jason',
'Joe','John','Jonathan','Joseph','Joshua','Julian','Justin','Keith','Kevin','Leonard','Liam','Lucas','Luke','Matt','Max','Michael','Nathan','Neil','Nicholas','Oliver','Owen','Paul','Peter','Phil','Piers','Richard','Robert',
'Ryan','Sam','Sean','Sebastian','Simon','Stephen','Steven','Stewart','Thomas','Tim','Trevor','Victor','Warren','William']

test_lastnames = ['Abraham','Allan','Alsop','Anderson','Arnold','Avery','Bailey','Baker','Ball','Bell','Berry','Black','Blake','Bond','Bower','Brown','Buckland','Burgess','Butler','Cameron','Campbell','Carr','Chapman','Churchill',
'Clark','Clarkson','Coleman','Cornish','Davidson','Davies','Dickens','Dowd','Duncan','Dyer','Edmunds','Ellison','Ferguson','Fisher','Forsyth','Fraser','Gibson','Gill','Glover','Graham','Grant','Gray','Greene',
'Hamilton','Hardacre','Harris','Hart','Hemmings','Henderson','Hill','Hodges','Howard','Hudson','Hughes','Hunter','Ince','Jackson','James','Johnston','Jones','Kelly','Kerr','King','Knox','Lambert','Langdon','Lawrence',
'Lee','Lewis','Lyman','MacDonald','Mackay','Mackenzie','MacLeod','Manning','Marshall','Martin','Mathis','May','McDonald','McLean','McGrath','Metcalfe','Miller','Mills','Mitchell','Morgan','Morrison','Murray','Nash',
'Newman','Nolan','North','Ogden','Oliver','Paige','Parr','Parsons','Paterson','Payne','Peake','Peters','Piper','Poole','Powell','Pullman','Quinn','Rampling','Randall','Rees','Reid','Roberts','Robertson','Ross','Russell',
'Rutherford','Sanderson','Scott','Sharp','Short','Simpson','Skinner','Slater','Smith','Springer','Stewart','Sutherland','Taylor','Terry','Thomson','Tucker','Turner','Underwood','Vance','Vaughan','Walker','Wallace','Walsh',
'Watson','Welch','White','Wilkins','Wilson','Wright','Young']