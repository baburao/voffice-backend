#from django.http import HttpResponse, JsonResponse
import json

from VOfficeApp.models import User
from VOfficeApp.room_handler import RoomHandler
from VOfficeApp.submodels.company import Company, GroupCompany
from VOfficeApp.submodels.room import Room, RoomTemplate


from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response


class CreateMeetingRoomView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    
    def post(self, request):
        
        name = request.data.get('name')
        size = request.data.get('size')
        
        new_room = Room.objects.create(name=name, size=size, type=Room.MEETING_ROOM)
        new_room.save()
        
        return Response(new_room.to_json())
    
class ListMeetingRoomsView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        answer = {}
        
        meeting_rooms = Room.objects.filter(room_type=Room.MEETING_ROOM)
        
        for meeting_room in meeting_rooms.all():
            answer[meeting_room.id] = meeting_room.to_json()
        
        return Response(answer)

"""
def addRoom(request):
    
    jsonData = json.loads(request.POST['data'])
   
    room_type_name = jsonData['type']
    room_name = jsonData['name']
    template_id = jsonData['template']
    width = jsonData['width']
    height = jsonData['height']
    
    template = RoomTemplate.objects.get(id=template_id)
    
    
    room_type = ''
    
    if room_type_name == 'meeting':
        room_type = Room.MEETING_ROOM
    elif room_type_name == 'reception': 
        room_type = Room.RECEPTION_ROOM
    else:   
        return HttpResponse(status=500)
        
    
    root_company = Company.objects.first()
        
    new_room = Room.objects.create(
        name=room_name, 
        room_type=room_type,
        room_template=template, 
        company=root_company,
        width=width,
        height=height
    )
    new_room.save()
    
    RoomHandler.instance.broadcastOfficeUpdate();
    
    return HttpResponse(status=200)

def addRoomTemplate(request):
    
    jsonData = json.loads(request.POST['data'])
   
    temp_name = jsonData['name']
    color_one = jsonData['color_one']
    color_two = jsonData['color_two'] 
        
    new_template = RoomTemplate.objects.create(name=temp_name, color_one=color_one, color_two=color_two)
    new_template.save()
    
    return HttpResponse(status=200)

def updateRoomTemplate(request):
    
    jsonData = json.loads(request.POST['data'])
   
    temp_id = jsonData['template']
    color_one = jsonData['color_one']
    color_two = jsonData['color_two'] 
        
    temp = RoomTemplate.objects.get(id=temp_id)
    temp.color_one = color_one
    temp.color_two = color_two
    temp.save()
    
    RoomHandler.instance.broadcastOfficeUpdate();
    
    return HttpResponse(status=200)

def listRoomTemplates(request):
    
    
    answer = {  
        'templates' : {}
    }
    for temp in RoomTemplate.objects.all(): 
        answer['templates'][temp.id] = {
            'name': temp.name,
            'color_one': temp.color_one,
            'color_two': temp.color_two,
        }
    return JsonResponse(answer)

def assignRoomTemplate(request):
    
    jsonData = json.loads(request.POST['data'])
   
    room_id = jsonData['room']
    template_id = jsonData['template']
     
    room = Room.objects.get(id=room_id)
    template = RoomTemplate.objects.get(id=template_id)
    
    room.room_template = template    
    room.save()
    
    RoomHandler.instance.broadcastOfficeUpdate();
    
    return HttpResponse(status=200)
"""
#helper functions for getOffice()
def appendUserIds(level, data):
    
    assignments = level.employeeassignment_set;
    
    data['workspaces'] = [-1] * level.room.size
    data['heads'] = {}
    
    for u in assignments.all():  
        user =  u.employeedata.userdata.user     
        
        if u.department_head:
            data['heads'][user.id] = True

        data['workspaces'][u.workspace] = user.id
    
        
def appendOffices(level, rooms):
    for u in level.employeeassignment_set.all():  
        
        user =  u.employeedata.userdata.user    
        room =  u.room
        
        if room is None:
            continue         
        rooms[room.id] = room.to_json()
        rooms[room.id]['owner'] = user.id       
        
def appendRooms(level, rooms):
    for room in level.rooms.all():    
        rooms[room.id] = room.to_json()

class GetOfficeView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        answer = {  
            'rooms' : {}
        }
        #todo which office
        is_root_group = True
        root_company = None
        
        company_id = 3
        
        if is_root_group:
            root_company = Company.objects.first()
        else:
            root_company = GroupCompany.objects.get(id=company_id)
            
        if root_company is None:        
            return Response(answer)
        
        answer['name'] = root_company.name;
          
        appendOffices(root_company, answer['rooms'])  
        appendRooms(root_company, answer['rooms'])  
        
        for b in root_company.branch_set.all():
            
            appendRooms(b, answer['rooms'])  
            appendOffices(b, answer['rooms'])  
            
            for d in b.department_set.all():
                department_room = d.room.to_json()                 
               
                appendUserIds(d, department_room)   
                answer['rooms'][d.room.id] = department_room           
                
             
        
        return Response(answer)
     
class GetOwnDepartmentView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
        
        user = request.user
       
        if user.user_type != User.TYPE_EMPLOYEE:
            return Response(status=500)
        
        if user.userdata is None or user.userdata.employee_data is None:
            return Response({'id' : -1})
                  
        assignment = user.userdata.employee_data.assignment
        
        if assignment is None:
            return Response({'id' : -1})
                  
        if not assignment.department is None:            
            
            department_room = assignment.department.room.to_json() 
            appendUserIds(assignment.department, department_room)
            return Response(department_room)
        else:
            return Response({'id' : -1})
          
            
class EnterRoomView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    
    def post(self, request):
           
        room_id = request.data.get('room')
        RoomHandler.instance.enterRoom(request.user.id, room_id, -1)
        
        return Response()

#enter Department workspace or own office Room
class EnterWorkspaceView(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated] 
    
    def post(self, request):
    
        user = request.user
       
        if user.user_type != User.TYPE_EMPLOYEE:
            return Response(status=500)
            
        assignment = user.userdata.employee_data.assignment
        room_id = -1
        slot_index = 0
        
        if not assignment.department is None:
            room_id = assignment.department.room_id
            slot_index = assignment.workspace
        else:
            if assignment.room is None:
                return Response(status=500)
            
            room_id = assignment.room_id
                
        RoomHandler.instance.enterRoom(user.id, room_id, slot_index)
        
        return Response()

class GetOfficeStatusView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):
    
        return Response(RoomHandler.instance.userToRoomMapping)

class SaveOfficeView(APIView):
   
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        rooms = request.data.get('rooms')
        
        for r in rooms:
            
            room = Room.objects.get(id=rooms[r]['id'])
            room.x = rooms[r]['x']
            room.y = rooms[r]['y']
            room.width = rooms[r]['width']
            room.height = rooms[r]['height']
            room.save();
        
        
        
        return Response()