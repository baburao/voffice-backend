from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *
from VOfficeApp.submodels.company import *
from VOfficeApp.submodels.userdata import *
from VOfficeApp.submodels.room import *
from VOfficeApp.submodels.files import *
# Register your models here.
#admin.site.register(User, UserAdmin)
admin.site.register(User)
admin.site.register(UserData)
admin.site.register(EmployeeData)
admin.site.register(EmployeeInformation)
admin.site.register(ClientData)
admin.site.register(VendorData)
admin.site.register(GuestData)

admin.site.register(RoleGroup)
admin.site.register(Designation)

admin.site.register(Company)
admin.site.register(GroupCompany)
admin.site.register(Branch)
admin.site.register(Department)
admin.site.register(EmployeeAssignment)

admin.site.register(ProfileImage)
admin.site.register(Notification)

admin.site.register(Room)
admin.site.register(RoomTemplate)

#admin.site.register(UploadFolder)
#admin.site.register(FileUpload)
#admin.site.register(FileShareUsers)

admin.site.register(Conversation)
admin.site.register(Message)
admin.site.register(NotRead)
#admin.site.register(UserToConversation)
admin.site.register(CallId)
admin.site.register(Conference)
#admin.site.register(AudioMessage)

    
    