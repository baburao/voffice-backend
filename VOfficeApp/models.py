from django.db import models
from django.utils.translation import gettext_lazy as _

from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver

from VOfficeApp.submodels.userdata import UserData
from VOfficeApp.submodels.files import FileUpload
from VOfficeApp.submodels.conversation import *
from django.contrib.auth.models import Group
#from VOfficeApp.submodels.room import *
   
def defaultUserData():
    new_data = UserData.objects.create()
    new_data.save()
    
    return new_data.id

office_permissions = [
            ("office_edit", "Manage Departments/Branches"),
            ("employee_edit", "Assign to Department/Branch, edit Details"),
            ("employee_add", "Add Employee"),
        ]

class RoleGroup(models.Model):
    """
    Holds all permissions for a role
    """
    group = models.OneToOneField(Group, on_delete=models.CASCADE)
    description = models.CharField(max_length=1024)

class Designation(models.Model):    
    """
    Designation for a User. A designation has multiple roles
    """
    class Meta:
        permissions = office_permissions
        
    name = models.CharField(max_length=150)
    description = models.CharField(max_length=1024)
    roles = models.ManyToManyField(RoleGroup)
    

class User(AbstractUser):
    
    TYPE_EMPLOYEE = 'e'
    TYPE_GUEST = 'g'
    TYPE_CLIENT = 'c'
    TYPE_VENDOR = 'v'
    
    USER_TYPE_CHOICES = (
        (TYPE_EMPLOYEE, 'employee'),
        (TYPE_GUEST, 'guest'),
        (TYPE_CLIENT, 'client'),
        (TYPE_VENDOR, 'vendor'),
    )
    
    USERNAME_FIELD = 'email'   
    email = models.EmailField(_('email address'), unique=True)
    #we cant get rid of the username for reasons
    username = models.CharField(_('username'), max_length=150, blank=True, null=True)
    
    user_type = models.CharField(
        _('usertype'),
        max_length=2,
        choices=USER_TYPE_CHOICES,
        default=TYPE_EMPLOYEE,
    )
    
    designation = models.ForeignKey(Designation, on_delete=models.SET_NULL, null=True, blank=True) 
    
    #user_data = models.OneToOneField(UserData, on_delete=models.CASCADE, null=True, default=defaultUserData)
    #user_data = models.OneToOneField(UserData, on_delete=models.CASCADE, null=True)
    
    REQUIRED_FIELDS = ['username']
    
    def toJSON(self):
        
        profile_image_id = -1
        if self.userdata.profile_image is not None:
            profile_image_id = self.userdata.profile_image_id
        
        return {
            'id': self.id,
            #'email': user.email,
            'firstname': self.first_name,
            'lastname': self.last_name,
            'profileImage' : profile_image_id,
            'designation': self.designation_id
        }
    
    def __str__(self):
        return '%s(%i)' % (self.email, self.id) 
    

@receiver(post_save, sender=User)
def create_user_data(sender, instance, created, **kwargs):
    if created:
        UserData.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_data(sender, instance, **kwargs):
    instance.userdata.save()
    
def user_type_from_string(type_string):
    if type_string == 'employee':
        return User.TYPE_EMPLOYEE
    elif type_string == 'guest':
        return User.TYPE_GUEST
    elif type_string == 'client':
        return User.TYPE_CLIENT
    elif type_string == 'vendor':
        return User.TYPE_VENDOR
    
    return ''