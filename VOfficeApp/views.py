#from django.shortcuts import render

from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
import json

from django.shortcuts import redirect

from django.db import connection


"""
#@login_required(login_url='/login/')
def indexView(request):
    
    template = loader.get_template('VOfficeApp/index.html')
    context = {}
    return HttpResponse(template.render(context, request))
"""
#@login_required(login_url='/login/')
def testView(request, page):
    
    template = loader.get_template('VOfficeApp/test_' + page + '.html')
    context = {}#'user_id': request.user.id, 'queries' : connection.queries}
    return HttpResponse(template.render(context, request))    
"""    
def logoutView(request):
    
    logout(request)
    return redirect('/login')

 
def getUser(request):
    
    if request.user.is_authenticated:
        return JsonResponse(request.user.toJSON())
    
    return JsonResponse({'id': -1})

@require_http_methods(["POST"])   
def userLogin(request):
    
    data = json.loads(request.body)
    email = data['email']
    password = data['password']
    #user_id = request.POST['user']
    user = authenticate(request, email=email, password=password)
   
    if user is not None:
        login(request, user)

        return JsonResponse(user.toJSON())
        ...
    else: 
   
        return HttpResponse(status=403)
"""