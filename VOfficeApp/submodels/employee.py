from django.db import models

class EmployeeInformation(models.Model):
    """
    Holds all personal information about an employee, family, financial, etc.
    """
    bio = models.TextField(blank=True)
    contact_number = models.CharField(max_length=20,blank=True)
    
    #def __str__(self):
        
    #    if self.employeedata == None:
    #        return 'EmployeeInformation'
    #    return '%s' % (self.employeedata.userdata.user.email )