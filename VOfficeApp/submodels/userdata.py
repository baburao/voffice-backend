from django.db import models
from VOfficeApp.submodels.company import EmployeeAssignment
from VOfficeApp.submodels.employee import EmployeeInformation
import ntpath

from django.core.files import File

from PIL import Image
from io import BytesIO

class EmployeeData(models.Model):
    """
    All data necessary for an employee type user    
    """
    placeholder = models.CharField(max_length=150)
    
    assignment = models.OneToOneField(EmployeeAssignment, on_delete=models.SET_NULL, blank=True, null=True)
    employee_information = models.OneToOneField(EmployeeInformation, on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        try:
            return '%s' % (self.userdata.user.email)
        except UserData.DoesNotExist:
            
            return 'EmployeeData'
    
class ClientData(models.Model):
    """
    All data necessary for a client type user    
    """
    placeholder = models.CharField(max_length=150)
    
class VendorData(models.Model):
    """
    All data necessary for a vendor type user    
    """
    placeholder = models.CharField(max_length=150)
    
class GuestData(models.Model):
    """
    All data necessary for a guest type user    
    """
    placeholder = models.CharField(max_length=150)
    
def image_upload_path(instance, filename):

    print(filename)
    #return 'uploads/{0}/{1}'.format(instance.owner.id, uuid.uuid4())
    return 'avatars/original/{0}'.format(filename)

def image_upload_path_small(instance, filename):

    return 'avatars/small/{0}'.format(ntpath.basename(filename))

def image_upload_path_big(instance, filename):

    return 'avatars/big/{0}'.format(ntpath.basename(filename))

def convertImage(image, size=(100, 100)):

    pil_image = Image.open(image)

    pil_image.convert('RGB') 
    pil_image.thumbnail(size) 
    image_data = BytesIO()
    pil_image.save(image_data, 'PNG')

    print(image.name)
    output_image = File(image_data, name=image.name) 

    return output_image

class ProfileImage(models.Model):
    """
    Holds a profile image, and all necessary sizes
    """
    original_image = models.ImageField(upload_to=image_upload_path, blank=True, null=True)
    small_image = models.ImageField(upload_to=image_upload_path_small, blank=True, null=True, default=None)
    big_image = models.ImageField(upload_to=image_upload_path_big, blank=True, null=True, default=None)
    
    def save(self, *args, **kwargs):
        
        if self.original_image != None:
            if self.small_image == None:            
                self.small_image = convertImage(self.original_image)
            if self.big_image == None:            
                self.big_image = convertImage(self.original_image, (400, 400))
        
        super().save(*args, **kwargs)
         
class UserData(models.Model):
    """
    Holds all types of user data
    Usually a user has only one type of data, and other fields are null, but we can store data in case a user changes types, e.g. guest gets upgraded to client.
    """
    user = models.OneToOneField('User', on_delete=models.CASCADE)
    
    employee_data = models.OneToOneField(EmployeeData, on_delete=models.SET_NULL, blank=True, null=True)
    vendor_data = models.OneToOneField(VendorData, on_delete=models.SET_NULL, blank=True, null=True)
    client_data = models.OneToOneField(ClientData, on_delete=models.SET_NULL, blank=True, null=True)
    guest_data = models.OneToOneField(GuestData, on_delete=models.SET_NULL, blank=True, null=True)
    
    last_room = models.ForeignKey('Room', on_delete=models.SET_NULL, null=True, blank=True, help_text="Id of the last room this user was in") 
    #last_call = models.ForeignKey('Call', on_delete=models.SET_NULL, null=True, blank=True) 
    
    profile_image = models.OneToOneField(ProfileImage, on_delete=models.SET_NULL, blank=True, null=True)
    
    
    def __str__(self):
        
        if self.user == None:
            return 'UserData'
        return '%s(%s)' % (self.user.email , self.user.user_type)
    
    
class Notification(models.Model):
    """
    A user notification
    """
    class Meta:
        indexes = [
            models.Index(fields=['user', 'user']),
            models.Index(fields=['has_read', 'has_read']),
        ]
        
    #TYPE_TEXT = 't'
    TYPE_CONFERENCE_INVITATION = 'ci'
    TYPE_MISSED_CALL = 'mc'
    TYPE_NEW_MESSAGE = 'nm'
    
    NOTIFICATION_TYPE_CHOICES = (
        (TYPE_CONFERENCE_INVITATION , 'conference_invitation'),
        (TYPE_MISSED_CALL , 'missed_call'),
        (TYPE_NEW_MESSAGE , 'new_message'),
    )
    notification_type = models.CharField(
        max_length=2,
        choices=NOTIFICATION_TYPE_CHOICES,
        default=TYPE_CONFERENCE_INVITATION,
    )
    
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    
    date = models.DateTimeField(auto_now_add=True)   
    
    target_id = models.IntegerField(default=-1, help_text="Depending on the notification type, this holds the id of the target. Conference for conference_invitation, VOfficeApp.Call for missed_call")            
    source_id = models.BigIntegerField(default=-1, help_text="Source id of this notification, usually a User id")            
    has_read = models.BooleanField(default=False, help_text="Flag, if the user has read this notification")
    
    def to_json(self):
        return { 
            'id': self.id,
            'type': self.notification_type,
            'target': self.target_id,
            'source': self.source_id,
            'date': str(self.date),
            'has_read': self.has_read
        }
