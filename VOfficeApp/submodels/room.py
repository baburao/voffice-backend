from django.db import models
from django.utils.translation import gettext_lazy as _

#

class RoomTemplate(models.Model):
    """
    Can hold template informations about certain Rooms.
    Not used at the moment!
    
    """
    name = models.CharField(max_length=128, default="")
    
    #todo actuall fields we need here
    color_one = models.CharField(max_length=7, default="#000000")
    color_two = models.CharField(max_length=7, default="#000000")
    

class Room(models.Model):
    """
    Visual Data of a room in the office view.
    """
    OFFICE_ROOM = 'of'
    DEPARTMENT_ROOM = 'de'
    MEETING_ROOM = 'me'
    RECEPTION_ROOM = 're'
    
    ROOM_TYPE_CHOICES = (
        (OFFICE_ROOM, 'office'),
        (DEPARTMENT_ROOM, 'department'),
        (MEETING_ROOM, 'meeting'),
        (RECEPTION_ROOM, 'reception'),
    ) 
    
    name = models.CharField(max_length=128, default="")
    
    room_type = models.CharField(
        _('room_type'),
        max_length=2,
        choices=ROOM_TYPE_CHOICES,
        default=OFFICE_ROOM,
    )
        
    #width = models.IntegerField(default=2)
    #height = models.IntegerField(default=2)
    
    size = models.IntegerField(default=8, help_text="The number of desks in this room, get increased automatically when employees are assigned")
    x = models.IntegerField(default=0, help_text="X position on the office view grid")
    y = models.IntegerField(default=0, help_text="Y position on the office view grid")
    width = models.IntegerField(default=2)
    height = models.IntegerField(default=2)
    
    
    room_template = models.ForeignKey(RoomTemplate, on_delete=models.SET_NULL, null=True)
    
    
    #For not owned Rooms, MEETING_ROOM,RECEPTION_ROOM
    branch = models.ForeignKey('Branch',related_name='rooms', on_delete=models.SET_NULL, null=True, blank=True, help_text="For office rooms, not used right now!")   
    group_company = models.ForeignKey('GroupCompany',related_name='rooms', on_delete=models.SET_NULL, null=True, blank=True, help_text="For office rooms, not used right now!")   
    company = models.ForeignKey('Company',related_name='rooms', on_delete=models.SET_NULL, null=True, blank=True, help_text="For office rooms, not used right now!")   
    
    def getTemplateId(self):
        if self.room_template_id is not None:
            return self.room_template_id 
        return -1
    
    def to_json(self):
        from VOfficeApp.room_handler import RoomHandler
        return {
            'id' : self.id,
            'type' : self.room_type,
            'name' : self.name,
            'size' : self.size,
            'template': self.getTemplateId(),
            'slots': RoomHandler.getSlots(self.id),
            'workspaces' : None,
            'x': self.x,
            'y': self.y,
            'width': self.width,
            'height': self.height,
        }
        
    def __str__(self):
        return '%s(%i)' % (self.room_type, self.id)
    
    
    