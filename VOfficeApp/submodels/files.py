from django.db import models
import uuid

def upload_path(instance, filename):

    #return 'uploads/{0}/{1}'.format(instance.owner.id, uuid.uuid4())
    return 'uploads/{0}/{1}'.format(instance.owner.id, filename)

class UploadFolder(models.Model):
    """
    Not used at the moment
    """
    path = models.CharField(max_length=1024)
    owner = models.ForeignKey('User', on_delete=models.SET_NULL, null=True)  
    
    shared_users = models.OneToOneField('FileShareUsers', on_delete=models.SET_NULL, null=True) 
    
class FileUpload(models.Model):
    """
    Not used at the moment
    """
    owner = models.ForeignKey('User', on_delete=models.SET_NULL, null=True)    
    
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=1024, default="" , blank=True)
   
    folder = models.ForeignKey(UploadFolder, on_delete=models.SET_NULL,blank=True, null=True)
    
    file = models.FileField(upload_to=upload_path)
    
    #sharing
    shared_users = models.OneToOneField('FileShareUsers', on_delete=models.SET_NULL, null=True)
    
class FileShareUsers(models.Model):
    """
    Not used at the moment
    """
    users = models.ManyToManyField('User') 

    departments = models.ManyToManyField('Department') 
    branches = models.ManyToManyField('Branch')     
    
 
#Helper function
def createFileEntry(request):
    
    upload_file = request.FILES['file']
    #description = request.POST['description']
    path = request.POST['path']
    
    folder = None
    if len(path) > 0:        
        folder_query = UploadFolder.objects.filter(owner=request.user, path=path)
        if not folder_query.exists():        
            folder = UploadFolder.objects.create(owner=request.user, path=path)
            folder.save()
            
    
    file = FileUpload.objects.create(owner=request.user, name=upload_file.name, file=upload_file, folder=folder, description='')
    
    file.save()
    
    return file