from rest_framework.views import APIView
from VOfficeApp.lib.company_db import execute_function
from VOfficeApp.lib import company_functions
from django.http import HttpResponse
import traceback
import json
from VOfficeApp.lib.logger import *




class GetSubCompanies(APIView):

    def get(self, request):
        try:
            log_request(request)
            # to get company db connection
            company_id = request.query_params.get('company_id')
            execute_function.get_connection_by_company(company_id)

            params = {'ip_company_id' : company_id}
            result = execute_function.get(params, company_functions.fn_get_sub_companies)
            log_result(result)
            return HttpResponse(json.dumps(result))

        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()


class ADDSubCompanies(APIView):

    def post(self, request):
        try:
            #     log_request(request)
            #     # to get company db connection
            company_id = request.data.get('company_id')
            execute_function.get_connection_by_company(company_id)
            #
            id = request.data.get('ip_id')
            name = request.data.get('ip_name')
            # company_id = request.data.get('ip_company_id')
            # group_company_id = request.data.get('ip_group_company_id')
            # address = request.data.get('ip_address')
            # phone_number = request.data.get('ip_phnoe_number')
            # business_name = request.data.get('ip_business_name')
            # business_email = request.data.get('ip_business_email')
            # business_phone_number = request.data.get('ip_business_phone_number')
            # legal_name = request.data.get('ip_legal_name')
            # legal_email = request.data.get('ip_legal_email')
            # legal_phone_number = request.data.get('ip_legal_phone_number')
            # corporate_name = request.data.get('ip_corporate_name')
            # corporate_email = request.data.get('ip_corporate_email')
            # corporate_phone_number = request.data.get('ip_corporate_phone_number')
            # career_name = request.data.get('ip_career_name')
            # career_email = request.data.get('ip_career_email')
            # career_phone_number = request.data.get('ip_career_phone_number')
            # branch_head = request.data.get('ip_branch_head')
            # active = request.data.get('ip_active')

            params = {
                        'ip_company_id': company_id
                       ,'ip_id': id
                       ,'ip_name': name
                      #  'ip_group_company_id': group_company_id,
                      #  'ip_address': address
                      #   ,'ip_phnoe_number':phone_number,
                      #  'ip_business_name': business_name,
                      # 'ip_business_email': business_email,
                      # 'ip_business_phone_number': business_phone_number
                      # ,'ip_legal_name': legal_name,
                      # 'ip_legal_email': legal_email,
                      # 'ip_legal_phone_number': legal_phone_number,
                      # 'ip_corporate_name': corporate_name,
                      # 'ip_corporate_email': corporate_email,
                      # 'ip_corporate_phone_number': corporate_phone_number,
                      # 'ip_career_name': career_name
                      # ,'ip_career_email': career_email,
                      # 'ip_career_phone_number': career_phone_number,
                      # 'ip_branch_head': branch_head,
                      # 'ip_active': active
                      }
            # params = {  'designation_details': json.dumps(designation_details)}
            # execute_function.put(params, company_functions.fn_ins_upd_designation)
            # return HttpResponse(json.dumps("Designation added successfully."))
            result = execute_function.put(params, company_functions.fn_ins_upd_subcompanies)

            return HttpResponse(result)
        except Exception as err:
            http_err = traceback.format_exc()
            print(http_err)
            return HttpResponse(http_err, status=500)

        finally:

            #     # to close company db connection
            execute_function().close_db_connection()