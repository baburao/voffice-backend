"""
Name                       : Employee
Description                : Employee related apis
Created By                 : Baburao M
Created Date               : 05/11/2020
Last Modified By           :
Last Modified Date         :
Modification Description   :
"""
from rest_framework.views import APIView
from VOfficeApp.lib.company_db import execute_function
from VOfficeApp.lib import company_functions
from django.http import HttpResponse
import traceback
import json
from VOfficeApp.lib.logger import *
from VOfficeApp.lib.email import send_mail
import datetime
from datetime import date

from django.contrib import auth
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.db import models
from django.db.models.manager import EmptyManager
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.hashers import make_password




class CreateEmployee(APIView):

    def post(self, request):
        try:
            log_request(request)
            # to get company db connection
            company_id = request.data['empofficeid']
            firstname = request.data['firstname']
            lastname = request.data['lastname']
            nickname = request.data['nickname']
            # password = "123"
            # data1 = make_password(password)
            passwords = make_password(request.data['passwords'])
            superuser = 'false'
            staff = 'false'
            datejoin = request.data['datejoin']
            emailid = request.data['emailid']
            usernames = 'user'
            usertype = 'e'
            designationid = 1
            dateofbirth = request.data['dateofbirth']
            gender1 = request.data['gender1']
            meritalstatus = request.data['meritalstatus']
            haskids = request.data['haskids']

            phonenumber = request.data['phonenumber']
            emercontactname = request.data['emercontactname']
            emercontactnum = request.data['emercontactnum']
            daughters = request.data['daughters']
            sons = request.data['sons']
            marriagedate = request.data['marriagedate']
            empuserid = request.data['empuserid']
            rolesjson = request.data['rolesjson']
            execute_function.get_connection_by_company(company_id)
            time_stamp = datetime.datetime.now().strftime("%Y-%m-%d")
            params = {'empofficeid' : company_id,'firstname':firstname,'lastname':lastname,'nickname':nickname,
                      'passwords' : passwords,'superuser':superuser,'staff':staff,'datejoin':datetime.datetime.now().strftime("%Y-%m-%d"),
                      'emailid' : emailid,'usernames':usernames,'usertype':usertype,'designationid':designationid,
                      'dateofbirth' : datetime.datetime.now().strftime("%Y-%m-%d"),'gender1':gender1,'meritalstatus':meritalstatus,'haskids':haskids,
                      'phonenumber' : phonenumber,'emercontactname':emercontactname,'emercontactnum':emercontactnum,
                      'daughters':daughters,
                      'sons' : sons,'marriagedate':datetime.datetime.now().strftime("%Y-%m-%d"),'empuserid':empuserid,'rolesjson':json.dumps(rolesjson)}
            result = execute_function.put(params, company_functions.fn_add_employee)
            SendEmailToEmployee(result,company_id,firstname,emailid)
            info_logger.info(result)
            return HttpResponse(result)

        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()

def SendEmailToEmployee(employeeid,companyid,firstname,emailid):
    try:
        loginurl = 'http://localhost:4200/activeemployee/'+str(companyid)+'/'+str(employeeid)
        templatename = 'registration_template.html'
        username = firstname
        replacetext = {'username': username.title(), 'Email Address': emailid, 'passwordtext': 'baburao',
                       'loginurl': loginurl}
        mail_status = send_mail.send_email(emailid, replacetext, templatename)
    except Exception as err:
        http_err = traceback.format_exc()
        return HttpResponse(http_err, status=500)

    finally:
        # to close company db connection
        execute_function().close_db_connection()

class GetCompanyEmployees(APIView):

    def get(self, request):
        try:
            log_request(request)
            #data = set_password('test')
            # to get company db connection
            company_id = request.query_params.get('company_id')
            execute_function.get_connection_by_company(company_id)
            params = {'ip_company_id' : company_id}
            result = execute_function.get(params, company_functions.fn_get_company_employees)
            log_result(result)
            # return HttpResponse(result)
            return HttpResponse(json.dumps(result))


        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()

class DeleteEmployee(APIView):

    def post(self, request):
        try:
            log_request(request)
            company_id = request.data['companyid']
            execute_function.get_connection_by_company(company_id)
            employeeid = request.data['employeeid']
            status = request.data['status']
            params = {'employeeid': employeeid,'isactive': status}
            result = execute_function.put(params, company_functions.fn_del_employee)
            log_result(result)
            return HttpResponse(json.dumps(result))


        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()

class ActivationEmailAddress(APIView):

    def post(self, request):
        try:
            log_request(request)
            company_id = request.data['companyid']
            execute_function.get_connection_by_company(company_id)
            employeeid = request.data['employeeid']
            params = {'employeeid': employeeid,'companyid': company_id}
            result = execute_function.put(params, company_functions.fn_activateemployeebymailid)
            log_result(result)
            return HttpResponse(json.dumps(result))
        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)
        finally:
            # to close company db connection
            execute_function().close_db_connection()

class GetEmployeeDataById(APIView):

    def post(self, request):
        try:
            log_request(request)
            # to get company db connection
            company_id = request.data['companyid']
            execute_function.get_connection_by_company(company_id)
            employeeid = request.data['employeeid']
            params = {'employeeid' : employeeid,'companyid':company_id}
            result = execute_function.get(params, company_functions.fn_getcompanyemployeedatabyid)
            log_result(result)
            return HttpResponse(json.dumps(result))


        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()

class ResendEmail(APIView):
    def post(self, request):
        try:
            company_id = request.data['companyid']
            execute_function.get_connection_by_company(company_id)
            employeeid = request.data['employeeid']
            loginurl = 'http://localhost:4200/activeemployee/'+str(company_id)+'/'+str(employeeid)
            templatename = 'registration_template.html'
            username = request.data['name']
            replacetext = {'username': username.title(), 'userid': 123, 'passwordtext': 'baburao',
                           'loginurl': loginurl}
            mail_status = send_mail.send_email('mannepalli62@gmail.com', replacetext, templatename)
            return HttpResponse(json.dumps(mail_status))
        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()

def set_password(raw_password):
    raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")


