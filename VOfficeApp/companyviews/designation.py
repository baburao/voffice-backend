import json

from rest_framework.views import APIView
from rest_framework.response import Response
from VOfficeApp.lib.company_db import execute_function
from VOfficeApp.lib import company_functions
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from django.http import HttpResponse
import traceback
from VOfficeApp.lib.logger import *

# class GetDesignation(APIView):
#
#     def get(self, request):
#
#         # to get company db connection
#         company_id = request.query_params.get('company_id')
#         execute_function.get_connection_by_company(company_id)
#
#         params = {'ip_company_id' : company_id}
#         result = execute_function.get(params, company_functions.fn_get_designation  )
#
#         # to close company db connection
#         execute_function().close_db_connection()
#         return Response(result)


class GetDesignation(APIView):

    def get(self, request):
        try:
            log_request(request)
            # to get company db connection
            company_id = request.query_params.get('company_id')
            execute_function.get_connection_by_company(company_id)

            params = {'ip_company_id' : company_id}
            result = execute_function.get(params, company_functions.fn_get_designation)
            log_result(result)
            return HttpResponse(json.dumps(result))

        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()

class AddDesignation(APIView):

    def post(self, request):
        try:
            log_request(request)
            # to get company db connection
            company_id = request.data.get('company_id')
            execute_function.get_connection_by_company(company_id)

            id = request.data.get('ip_designation_id')
            designation_name = request.data.get('ip_designation_name')
            description = request.data.get('ip_description')
            company_id = request.data.get('ip_company_id')
            active = request.data.get('ip_active')
            params = {'ip_company_id' : company_id, 'ip_designation_id': id,
                      'ip_designation_name' :designation_name,'ip_description':description,
                       'ip_company_id' :company_id,'ip_active':active }
            # params = {  'designation_details': json.dumps(designation_details)}
            # execute_function.put(params, company_functions.fn_ins_upd_designation)
            # return HttpResponse(json.dumps("Designation added successfully."))
            result = execute_function.put(params, company_functions.fn_ins_upd_designation)

            return HttpResponse(result)
        except Exception as err:
            http_err = traceback.format_exc()
            print(http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()