"""
Name                       : Department
Description                : Department APIs
Created By                 : Bharathi
Created Date               : 31-11-2020
Last Modified By           :
Last Modified Date         :
Modification Description   :
"""


from rest_framework.views import APIView
from VOfficeApp.lib.company_db import execute_function
from VOfficeApp.lib import company_functions
from django.http import HttpResponse
import traceback
import json
from VOfficeApp.lib.logger import *




class GetDepartments(APIView):

    def get(self, request):
        try:
            log_request(request)
            # to get company db connection
            company_id = request.query_params.get('company_id')
            execute_function.get_connection_by_company(company_id)

            params = {'ip_company_id' : company_id}
            result = execute_function.get(params, company_functions.fn_get_departments)
            log_result(result)
            return HttpResponse(json.dumps(result))

        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            # to close company db connection
            execute_function().close_db_connection()

class AddDepartments(APIView):

    def post(self, request):
        try:
        #     log_request(request)
        #     # to get company db connection
            company_id = request.data.get('company_id')
            execute_function.get_connection_by_company(company_id)
        #
            id = request.data.get('ip_department_id')
            name = request.data.get('ip_department_name')
            branch_id = request.data.get('ip_branch_id')
            room_id = request.data.get('ip_room_id')
            # company_id = request.data.get('ip_company_id')
            active = request.data.get('ip_active')
            description = request.data.get('ip_description')
            params = {'ip_company_id' : company_id
                       ,'ip_department_id': id,
                      'ip_department_name' :name,
                      'ip_branch_id':branch_id,
                       'ip_room_id' :room_id,
                      # 'ip_company_id':company_id,
                      'ip_active':active,
                      'ip_description':description
                      }
            # params = {  'designation_details': json.dumps(designation_details)}
            # execute_function.put(params, company_functions.fn_ins_upd_designation)
            # return HttpResponse(json.dumps("Designation added successfully."))
            result = execute_function.put(params, company_functions.fn_ins_upd_department)

            return HttpResponse(result)
        except Exception as err:
            http_err = traceback.format_exc()
            print(http_err)
            return HttpResponse(http_err, status=500)

        finally:

        #     # to close company db connection
             execute_function().close_db_connection()