from rest_framework.views import APIView
from VOfficeApp.lib.logger import *
from VOfficeApp.lib.company_db import *
from django.http import HttpResponse
import traceback
from VOfficeApp.lib import company_functions
import json
# INSERTING / UPDATING  A ROLE INTO ROLES TABLE
class InsertUpdateCompanyroles(APIView):
    def post(self, request):
        try:
            log_request(request)
            role_name= request.data.get('role_name');
            company_id = request.data.get('ip_company_id');
            role_id = request.data.get('role_id');
            isactive = request.data.get('is_active');
            execute_function.get_connection_by_company(company_id)
            params = {
                'roleid': role_id,
                'rolename':role_name,
                'cmpy_id' : company_id,
                'isactive':isactive
            }
            result = execute_function.put(params, company_functions.fn_addCompyRole)
            if result ==0 :
                info_logger.info('Role Added')
                return HttpResponse()
                return HttpResponse(json.dumps('Role Added'))
            elif result ==2 :
                info_logger.info('Role Updated')
                return HttpResponse(json.dumps('Role Updated'))
            else :
                info_logger.info('Duplicate Record Found')
                return HttpResponse(json.dumps('Duplicate Record Found'))
        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            execute_function().close_db_connection()

# GETTING COMPANY ROLES BY COMPANY ID
class GetcmpyrolesbycmpyId(APIView):
    def get(self, request):
        try:
            log_request(request)
            company_id = request.query_params.get('company_id');
            execute_function.get_connection_by_company(company_id)
            params = {'ip_company_id': company_id}
            result = execute_function.get(params, company_functions.fn_getCompyRole)
            info_logger.info(result)
            return HttpResponse(json.dumps(result))

        except Exception as err:
            http_err = traceback.format_exc()
            log_error(request, http_err)
            return HttpResponse(http_err, status=500)

        finally:
            execute_function().close_db_connection()
