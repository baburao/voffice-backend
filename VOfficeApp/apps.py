from django.apps import AppConfig
import sys

class VofficeappConfig(AppConfig):
    name = 'VOfficeApp'


    def ready(self):
        from VOfficeApp.room_handler import RoomHandler
        
        if 'runserver' in sys.argv or '0.0.0.0' in sys.argv:        
            RoomHandler.init()        
            print("READY")