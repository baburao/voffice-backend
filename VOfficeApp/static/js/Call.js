var Call = function(inSocket) {

	var self = this;
	
	this.socket = inSocket;
	this.connections = [];
	this.screenShareStream = null;	
	this.callId = -1;
	
	this.onConnected = null;
	this.onLeave = null;
	
	
	this.init = async function() {
			
	}
	this.processSignal = function(data) {
		
		if(data.signal == 'join') {								
			
			//participants.push(data.user);
			//updateParticipants();
		} else if(data.signal == 'enter') {
			
			//participants = data.users;
			//updateParticipants();						
			self.callId = data.id;
			
			for(var i = 0;i < data.users.length;i++) {

				self.addConnection(data.users[i]);
			}
				
		} else if(data.signal == 'leave') {
			
			self.removeConnection(data.user);
		} else if(data.signal == 'offer') {
			
			self.addConnectionByOffer(data.id, data.data);						
		} else if(data.signal == 'answer') {
			
			self.setAnswer(data.id, data.data);						
		} else if(data.signal == 'ice') {
			
			self.setCandidate(data.id, data.data);						
		}
	}
	this.connected = function(connection, stream) {
		
		if(self.onConnected != null) {
			self.onConnected(connection, stream);
		}
	}
	
	this.addScreenShare = async function() {

		if(self.screenShareStream != null) {
			
			return;
		}
		self.screenShareStream = await self.initScreenShare();
		
		/*for(let i = 0; i < self.connections.length; i++) {
			self.connections[i].addScreenShare();
		}*/
	}
	this.addConnection = async function(id) {
		
		var newConnection = new CallConnection(id, self);
		self.connections.push(newConnection);
		newConnection.offerConnection();
		return newConnection;
	}
	this.addConnectionByOffer = async function(id , offer) {
		
		//self.removeConnection(message.id);
		
		var newConnection = new CallConnection(id, self);
		newConnection.setRemoteDescription(offer);
		self.connections.push(newConnection);
		
		return newConnection;
	}
	this.setAnswer = async function(id, answer) {
		
		var connection = self.getConnection(id);
		if(connection != null) {
			connection.setAnswer(answer);
		}	
	}
	this.setCandidate = async function(id, candidate) {
		
		var connection = self.getConnection(id);
		if(connection != null) {
			
			connection.peerConnection.addIceCandidate(candidate).catch(e => {
			      console.error("ICE error: ", e);
		    });
		}
	}
	this.getConnection = function(id) {
		
		var index = self.connections.findIndex((element) => element.targetId == id);
		
		if(index > -1) {
			return self.connections[index];
		}
		return null;
	}
	this.removeConnection = function(id) {
		
		var index = self.connections.findIndex((element) => element.targetId = id);
		
		var oldConnection = self.connections[index];
		if(index > -1) {
			self.connections.splice(index,1);
		}
		
		if(this.onLeave != null) {
			this.onLeave(oldConnection.targetId);
		}
		//.......
	}
	 this.initScreenShare = async function() {
		
		let width = 540;
		let height = 320;
		
	    if (navigator.getDisplayMedia) {
	      return navigator.getDisplayMedia({video: { width: width, height: height }});
	    } else if (navigator.mediaDevices.getDisplayMedia) {
	      return navigator.mediaDevices.getDisplayMedia({video: { width: width, height: height }});
	    } else {
	      return navigator.mediaDevices.getUserMedia({video: {mediaSource: 'screen', width: width, height: height}});
	    }		  
	}
}

Call.prototype.constructor = Call;