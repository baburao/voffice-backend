/**
 * 
 */
var CallConnection = function(inId, inCall) {
	
	
	var self = this;
	
	this.targetId = inId;
	this.peerConnection = null;
	this.videoElement = null;
	this.call = inCall;	
	
	this.init = async function() {
		
		if(self.peerConnection != null) {
			return;
		}
		/*
		self.videoElement = document.createElement("video");
		self.videoElement.style = "width: 50%;height: auto;border-radius: 1px;"
		self.videoElement.setAttribute("playsinline", true);				
		self.videoElement.setAttribute("controls", true);				
		self.videoElement.setAttribute("autoplay", true);				
		
		$("#videoElements").append(self.videoElement);
		*/
		const config = {
			iceServers: [{
				urls:"stun:stun.l.google.com:19302"
			}]		    
		};
		options = {};
				
		self.peerConnection = new RTCPeerConnection(config, options);	
		
		self.peerConnection.ontrack = function(e){

			if(e.streams.length > 0 ){
				
				console.log('onTrack', e.streams);
				//if (self.videoElement.srcObject !== e.streams[0]) {					
					
					self.call.connected(self, e.streams[0]);					
					//self.videoElement.srcObject = e.streams[0];
				//}
			}
		};

		self.peerConnection.onnegotiationneeded  = function(event) {
			//console.log('onnegotiationneeded ===========>');
		}
		
		self.peerConnection.oniceconnectionstatechange = function(event) {

			console.log('iceconnectionstatechange', self.peerConnection.iceConnectionState);
		};	
		
		self.peerConnection.onicecandidate = function(event) {

			//console.log(event.candidate);	
			
			if(event.candidate != null) {
				var message = {
		    			  type:"signal",
		    			  signal:"ice",
		    			  id: self.targetId,
		    			  data: event.candidate
		    	};
		    	self.send(message);
			}
		};	
		
		self.peerConnection.onerror = function(event) {	

			console.log('WebRTC Error', event);	
		};
		self.peerConnection.onclose = function(event) {
			console.log('Close', event);		
		};
		self.peerConnection.onremovestream = function(event) {
			console.log('onremovestream', event);	
		};	
		
		self.addScreenShare();
	}

	this.addScreenShare = function() {
		
		self.call.screenShareStream.getTracks().forEach(track => self.peerConnection.addTrack(track, self.call.screenShareStream));
	}
	this.offerConnection = async function() {
		
		await self.init();
		
		var offerOptions = {
				offerToReceiveAudio: 1,
				offerToReceiveVideo: 1,
				voiceActivityDetection: false
		};
		
		self.peerConnection.createOffer(offerOptions)
	      .then(function(desc){
	    	  
	    	self.peerConnection.setLocalDescription(desc);

			console.log("Offer Created",self.targetId, desc);
	    	var message = {
				type: "signal",
				signal: "offer",
				id: self.targetId,
				data: desc
	    	};
	    	self.send(message);
	    	  //console.log(message);
	      })
	      .catch(function(e){
	    	  console.log(e);
	      });
	}

	this.setRemoteDescription = async function(offer) {

		await self.init();
		
		self.peerConnection.setRemoteDescription(offer)
		.then(function () {

			console.log("Remote Offer received", offer);
	    	  
	    	self.peerConnection.createAnswer().then(function(answer) {
	    		  self.peerConnection.setLocalDescription(answer);

	  			console.log("Answer Created", answer);
		
		    	  var message = {
		    			  type:"signal",
		    			  signal: "answer",
		    			  id: self.targetId,
		    			  data: answer
		    	  };
		    	  self.send(message);
	    	}).catch(function(e){
		    	  console.log(e);	    	  
	    	});
	    	  
	    	  
		}).catch(function(e){
	    	  console.log(e);	    	  
	    });
	}
	this.setAnswer = async function(answer) {
		
		self.peerConnection.setRemoteDescription(answer)
		.then(function () {

	    	  console.log("set answer description");
		}).catch(function(e){
	    	  console.log(e);	    	  
	    });
	}	
	
	this.receiveConnection = async function(offerSDP) {
		
		await self.init();		
	}
	
	this.send = function(data) {
		if(self.call.socket != null) {
			self.call.socket.send(data);
		}
	}
}

CallConnection.prototype.constructor = CallConnection;
