function getData(name, loadEvent) {

	
	$.ajax({
		//url : window.location.href + name,
		url : '/' + name,
		method : 'GET',
		dataType : 'json',
		data : {
		//	csrfmiddlewaretoken : getCookie('csrftoken'),
		},
		success : function(response) {

			//console.log(response);
			loadEvent(response);

		},
		error : function(err) {

			console.log(err);
		}
	});
}

function sendData(name, data, successEvent) {
	
	$.ajax({
		url : '/' + name,
		method : 'POST',
		//dataType : 'json',
		data : {
			csrfmiddlewaretoken : getCookie('csrftoken'),
			data: JSON.stringify(data)
		}
	}).done(function(data) {

		successEvent(data);
	}).fail(function(e) {
			console.log(e);
	});
}

function uploadFile(file, path) {
		
	var fd = new FormData();
	
    fd.append('file', file); 
    fd.append('csrfmiddlewaretoken', getCookie('csrftoken')); 
    fd.append('description', 'desc'); 
    fd.append('path', path); 
        
	$.ajax({
		url : '/uploadFile',
		method : 'POST',
		data : fd,
		cache: false,
        contentType: false,
        processData: false
		
	}).done(function() {

	}).fail(function(e) {
			console.log(e);
	});
}
function sendFileMessage(file, path, conversation) {
	
	var fd = new FormData();
	
    fd.append('file', file); 
    fd.append('csrfmiddlewaretoken', getCookie('csrftoken')); 
    fd.append('description', 'desc'); 
    fd.append('path', path); 
    fd.append('conversation', conversation);
        
	$.ajax({
		url : '/sendFileMessage',
		method : 'POST',
		data : fd,
		cache: false,
        contentType: false,
        processData: false
		
	}).done(function() {

	}).fail(function(e) {
			console.log(e);
	});
}
//To get the csrf token for post requets
function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i].trim();
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie
						.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}
