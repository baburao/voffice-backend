var ServerConnection = function() {
	
	this.socket = null;
	this.reconnecting = false;
	this.active = true;
	

	this.onSignal = null;
	
	var self = this;
	
	this.connect = function() {

		self.socket = new WebSocket('ws://' + window.location.host + '/ws/office/');
		self.socket.binaryType = 'arraybuffer';	
		
		self.socket.addEventListener('open', function (event) {			
			self.reconnecting = false;
			

			console.log('WebSocket opened ', event);
			//self.send({test:"test"})
			
			//self.socket.send(JSON.stringify(message));
		});
		
		self.socket.addEventListener('message', function (event) {		
			
			var message = JSON.parse(event.data);		
			
				
			if(message.type == 'signal') {

				if(self.onSignal != null) {
					self.onSignal(message);
				}
				
			} else if(data.type == 'update') {
				
			}			
		});
		
		self.socket.addEventListener('error', function (event) {
			console.log('WebSocket Error ', event);
		});
		self.socket.addEventListener('close', function (event) {
			console.log('WebSocket closed ', event);
			
			if(self.active) {
				self.reconnect();
			}
		});
	}	
	
	this.close = function() {
		self.active = false;
		self.socket.close();
	}
	this.send = function(message) {
		
		self.socket.send(JSON.stringify(message));
	}
	
	this.reconnect = function() {
		
		if(self.reconnecting) {
			return;
		}
		self.reconnecting = true;
		setTimeout(self.connect, 1000);
	}
}

ServerConnection.prototype.constructor = ServerConnection;