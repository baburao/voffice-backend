"""
Name                       : Functions
Description                : To define function names
Created By                 : Vasanthi
Created Date               : 31-11-2020
Last Modified By           :
Last Modified Date         :
Modification Description   :
"""

# company schema
company_schema = 'public.'

# deparment functions by Bharathi
fn_get_departments = company_schema + 'fn_get_departments'
fn_ins_upd_department = company_schema + 'fn_ins_upd_department'


#employee functions by baburao
fn_add_employee = company_schema + 'fn_add_user'
fn_get_company_employees = company_schema + 'fn_get_company_employees'
fn_del_employee = company_schema + 'fn_del_employee'
fn_getcompanyemployeedatabyid = company_schema + 'fn_getcompanyemployeedatabyid'
fn_activateemployeebymailid = company_schema + 'fn_activateemployeebymailid'

# designation functions by Bharathi
fn_get_designation = company_schema + 'fn_get_designation'
fn_insandupd_designation = company_schema + 'fn_ins_upd_designation'

# COMAPANY ROLES FUNCTION NAMES
fn_addCompyRole = company_schema + 'fn_addupdatecompanyroles'
fn_getCompyRole = company_schema + 'fn_getcmpyroles'
fn_activedeactive = company_schema + 'fn_deactivaterole'
fn_ins_upd_designation = company_schema + 'fn_ins_upd_designation'


# login functions by Vasanthi
fn_get_user_permissions = company_schema + 'fn_get_user_permissions'
fn_get_company_id_by_domain = company_schema + 'fn_get_company_id_by_domain'

#branch functions by bharathi
fn_get_branch = company_schema + 'fn_get_branch'
fn_ins_upd_branch = company_schema + 'fn_ins_upd_branch'
#subcompanies functions by bharathi
fn_get_sub_companies = company_schema + 'fn_get_sub_companies'
fn_ins_upd_subcompanies = company_schema + 'fn_ins_upd_subcompanies'

