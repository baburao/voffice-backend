"""
Name                       : Logger
Description                : To log info and errors
Created By                 : Vasanthi
Created Date               : 31-11-2020
Last Modified By           :
Last Modified Date         :
Modification Description   :
"""
import logging

info_logger = logging.getLogger('info_logger')
error_logger = logging.getLogger('error_logger')

def log_request(request):
    info_logger.info(request.path)
    if request.query_params != {}:
        info_logger.info(request.query_params)
    if request.data != {}:
        info_logger.info(request.data)
    info_logger.info('')

def log_result(result):
    info_logger.info(result)
    info_logger.info('')

def log_error(request, http_err):
    error_logger.error(request.path)
    if request.query_params != {}:
        error_logger.error(request.query_params)
    if request.data != {}:
        error_logger.error(request.data)
    error_logger.error(http_err)
    error_logger.error('')

