"""
Name                       : Query Execute
Description                : It execute function and returns result.
Created By                 : Vasanthi
Created Date               : 31-11-2020
Last Modified By           :
Last Modified Date         :
Modification Description   :
"""
from django.db import connection
import psycopg2



class execute_function:

    def get_connection_by_company(company_id):
        global cursor
        global conn
        if company_id == 0:
            conn = connection
            cursor = connection.cursor()
        else:
            cursor = connection.cursor()
            params = {'ip_company_id' : company_id}
            # params['ip_company_id'] = 6
            cursor.callproc('public.fn_get_company_db_connection', params)

            result = cursor.fetchall()
            company_db_connection_details = result[0][0][0]
            server_database = company_db_connection_details['server_database']
            if server_database == True:
                conn = connection
            else:
                cursor.close()
                connection.close()
                conn = psycopg2.connect(company_db_connection_details['database_connection'])
                cursor = conn.cursor()

    def get(params, function_name):
        if params == {}:
            cursor.callproc(function_name)
        else:
            cursor.callproc(function_name, params)
        result = cursor.fetchall()
        result = result[0][0]
        if result is None:
            return []
        else:
            return result

    def put(params, function_name):
        if params == {}:
            cursor.callproc(function_name)
        else:
            cursor.callproc(function_name, params)
        conn.commit()
        result = cursor.fetchall()
        result = result[0][0]
        if result is None:
            return []
        else:
            return result

    def close_db_connection(self):
        cursor.close()
        conn.close()

    def execute_proc(params, function_name):
        if params == {}:
            cursor.execute("CALL "+ function_name)
        else:
            # cursor.execute("CALL "+ function_name, params)
            y = list(params.values())
            x = tuple(y)
            procparams= [];
            for range in y:
                procparams.append("%s")
            currenttuple = tuple(procparams)
            print(currenttuple)
            cursor.execute("CALL"+ function_name + ''.join(currenttuple) , x)
        conn.commit()
        result = cursor.fetchall()
        result = result[0][0]
        if result is None:
            return []
        else:
            return result

    def execute(args, query):
        cursor = connection.cursor()
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        rows = cursor.fetchall()
        cursor.close()
        result = rows[0][0]
        if result is None:
            return []
        else:
            return result




