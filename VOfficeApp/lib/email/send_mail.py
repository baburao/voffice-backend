
from django.core.mail import BadHeaderError, send_mail
from django.http import HttpResponse, HttpResponseRedirect
import smtplib
from smtplib import SMTPException
import json
from django.conf import settings
from django.template.loader import get_template
from django.template import Context,RequestContext
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from django.utils.html import strip_tags
from email.mime.image import MIMEImage


# server = "http://40.114.72.38:9090/"
# username = "sravanthi"
# password = "123456"
# project_key = "GC"
# issue_comment = "This is the fifth dummy issue"
# assignee = "venkatg"
# create_jira(server, username, password, project_key, issue_comment, assignee)





def send_email(toemailid,replacetext,templatefilename):
    try:
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('multipart')
        msg['From'] = settings.DEFAULT_FROM_EMAIL
        msg['To'] = toemailid
        msg['Subject'] = "Activation Mail !"
        recepient = [toemailid]
        template = get_template(templatefilename)
        context = Context(replacetext)
        body_html = template.render(replacetext)
        body_text = strip_tags(body_html)

        # part1 = MIMEText(body_text, 'plain')
        part2 = MIMEText(body_html, 'html')
        # msg.attach(part1)
        msg.attach(part2)
        print(replacetext)
        # This example assumes the image is in the current directory
        fp = open("C://projects//voffice-api//voffice-backend//VOfficeApp//templates//email_logo.png", 'rb')

        msgImage = MIMEImage(fp.read())
        fp.close()
        msgImage.add_header('Content-ID', '<logoimage>')
        msg.attach(msgImage)

        # Send the message via local SMTP server.
        smtpsrv = settings.EMAIL_HOST
        smtpserver = smtplib.SMTP(smtpsrv, settings.EMAIL_PORT)

        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        smtpserver.login(settings.DEFAULT_FROM_EMAIL, settings.EMAIL_HOST_PASSWORD)
        smtpserver.sendmail(settings.DEFAULT_FROM_EMAIL, recepient, msg.as_string())
        smtpserver.close()

    except Exception as err:
        print(err)
        return HttpResponse(err)
    else:
        return 'success'