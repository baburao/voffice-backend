# Virtual Office Development Setup
 
### Install Django

###### CMD

```
python -m pip install Django
```

* [Docs ](https://docs.djangoproject.com/en/3.0/topics/install/#installing-official-release)

### Setup Postresql


#### Install PostgreSQL
* [PostgreSQL for Windows](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)

###### CMD

```
pip install psycopg

```


#### Init Databse

###### open psql(start menu, search for psql):

```
CREATE DATABASE VOffice;
CREATE USER vofficeuser WITH PASSWORD 'admin';
ALTER ROLE vofficeuser SET client_encoding TO 'utf8';
ALTER ROLE vofficeuser SET default_transaction_isolation TO 'read committed';
ALTER ROLE vofficeuser SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE voffice TO vofficeuser;

```

### Migrate, needs to be called once for setup:
* MakeMigration.bat - to prepare migrations
* Migrate.bat - to apply migrations


###### Create superuser:
* Open CMD Window in backend folder(where manage.py is located)
* Execute:

```
python manage.py createsuperuser --username admin --email admin@admin.dev
```

	password= admin


###### Start server:

* StartServer.bat - start the development server at http://localhost:8000/

###### Create Test Users:
* open [http://localhost:8000/test/create](http://localhost:8000/test/create)
* click Create Users
* 10 test users will be created

	login: test[0-9]@test.com
	password: test

#### Reseting DB

* open psql(start menu, search for psql):

```
DROP DATABASE VOffice;
CREATE DATABASE VOffice;
GRANT ALL PRIVILEGES ON DATABASE voffice TO vofficeuser;

```

* Delete VOfficeApp\migrations folder
* run make migrations and migrate
* Create superuser(see above)
* Create test users


### Documentation

#### Install docutils
```
pip install django-docutils
pip install drf_yasg

```

#### Open Documentation

[http://localhost:8000/admin/doc/models](http://localhost:8000/admin/doc/models)
