from django.contrib import admin
from django.urls import include, path
from rest_framework import routers, serializers, viewsets
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.schemas import get_schema_view

from VOfficeApp.companyviews.branch import *
from VOfficeApp.companyviews.role import *
from VOfficeApp.companyviews.employee import *
from VOfficeApp.companyviews.designation import GetDesignation, AddDesignation
from VOfficeApp.companyviews.subcompanies import *
from VOfficeApp.models import User
# from VOfficeApp.subcompanies
from VOfficeApp.submodels.conversation import Conference

from VOfficeApp.subviews.conversation import *
from VOfficeApp.subviews.company import *
from VOfficeApp.subviews.user import *
from VOfficeApp.subviews.office import *
from VOfficeApp.subviews.management import *
from VOfficeApp.subviews.files import DownloadFileView

from VOfficeApp.companyviews.department import *
# Serializers define the API representation.
#class UserSerializer(serializers.HyperlinkedModelSerializer):
#    class Meta:
#        model = User
#        fields = ['url', 'id', 'username', 'email', 'first_name', 'last_name' ]

# ViewSets define the view behavior.
#class UserViewSet(viewsets.ModelViewSet):
#    queryset = User.objects.all()
#    serializer_class = UserSerializer
    

   
    
# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'voffice/designations', DesignationViewSet)
#router.register(r'conferences', ConferenceView)




urlpatterns = [
   
    
    #Just for development

    path('', include('VOfficeApp.urls')),
    path('admin/', admin.site.urls),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    
    path('', include(router.urls)),
    #path('voffice/', include('rest_framework.urls', namespace='rest_framework')),
    
    path('voffice/user/', GetUserView.as_view()),
    path('voffice/GetAdmin/', GetAdminView.as_view()),

    path('voffice/updateUser/', UpdateUserDataView.as_view()),
    path('voffice/changePassword/', UserChangePasswordView.as_view()),
    path('voffice/addEmployee/', AddEmployee.as_view()),
    path('voffice/assignDesignation/', AssignUserDesignationView.as_view()),
    
    # path('voffice/addDepartment/', AddDepartmentView.as_view()),
    path('voffice/addBranch/', AddBranchView.as_view()),
    
        
    path('voffice/notifications/', GetNotificationsView.as_view()),
    
    path('voffice/image/<slug:image_size>/<int:user_id>', ProfileImageView.as_view()),
    path('voffice/uploadAvatar/', UploadProfileImageView.as_view()),
    path('voffice/allNotifications/', GetAllNotificationsView.as_view()),
    path('voffice/readNotification/', ReadNotificationsView.as_view()),
    
    path('voffice/addConversation/', AddConversationView.as_view()),
    path('voffice/addToConversation/', AddToConversationView.as_view()),
    path('voffice/conversation/<int:conversation_id>', RequestConversationView.as_view()),
    path('voffice/sendMessage/', SendMessageView.as_view()),
    path('voffice/sendFileMessage/', SendFileMessageView.as_view()),
    path('voffice/sendAudioMessage/', SendAudioMessageView.as_view()),
    path('voffice/conversations/', ListConversationsView.as_view()),
    path('voffice/messageStatus/', GetMessageStatusView.as_view()),
    path('voffice/audio/<int:audio_id>', GetAudioMessageView.as_view()),
    
    path('voffice/call/', CallUserView.as_view()),
    path('voffice/declineCall/', DeclineCallView.as_view()),
    path('voffice/leaveCall/', LeaveCallView.as_view()),
    path('voffice/joinCall/', JoinCallView.as_view()),
    
    path('voffice/file/<int:file_id>', DownloadFileView.as_view()),
    
    path('voffice/createMeetingRoom/', CreateMeetingRoomView.as_view()),
    path('voffice/listMeetingRooms/', ListMeetingRoomsView.as_view()),
    
    path('voffice/listConferences/', ListConferencesView.as_view()),
    path('voffice/listAllConferences/', ListAllConferencesView.as_view()),
    path('voffice/listUsers/<slug:user_type_param>', ListUsersView.as_view()),
    path('voffice/saveConference', SaveConferenceView.as_view()),
    
    path('voffice/office/', GetOfficeView.as_view()),
    path('voffice/saveOffice/', SaveOfficeView.as_view()),
    path('voffice/enterWorkspace/', EnterWorkspaceView.as_view()),
    path('voffice/enterRoom/', EnterRoomView.as_view()),
    path('voffice/department/', GetOwnDepartmentView.as_view()),
    path('voffice/assignUser/', AssignUserView.as_view()),
    
    path('voffice/addTeam/', AddTeamView.as_view()),
    path('voffice/deleteTeam/', DeleteTeamView.as_view()),
    path('voffice/assignToTeam/', AssignToTeamView.as_view()),
    path('voffice/removeFromTeam/', RemoveFromTeamView.as_view()),
    
    path('voffice/listTeams/', ListTeamsView.as_view()),
    
    path('voffice/company/', GetCompanyView.as_view()),
    
    #path('voffice/login', obtain_auth_token, name='api_token_auth'),
    path('voffice/login/', UserLoginView.as_view()),
    path('voffice/logout/', UserLogoutView.as_view()),            
    path('voffice/resetPassword/', ResetPasswordView.as_view()),
    path('voffice/admin/getdesignation/', GetDesignation.as_view()),
    path('voffice/admin/adddesignation/', AddDesignation.as_view()),
    path('voffice/admin/createemployee/', CreateEmployee.as_view()),
    path('voffice/admin/getcompanyemployees/', GetCompanyEmployees.as_view()),
    path('voffice/admin/delemployee/', DeleteEmployee.as_view()),
    path('voffice/admin/getemployeedatabyid/', GetEmployeeDataById.as_view()),
    path('voffice/admin/activationemailaddress/', ActivationEmailAddress.as_view()),

    # COMPANY ROLES URL
    # path('voffice/admin/addcompanyroles/', InsertCompanyroles.as_view()),
    #path('voffice/admin/addcompanyroles/', InsertCompanyroles.as_view()),
    path('voffice/admin/getcmpyrolesbyid/',GetcmpyrolesbycmpyId.as_view()),
    path('voffice/admin/addupdatecompanyroles/', InsertUpdateCompanyroles.as_view()),
    path('voffice/admin/getcmpyrolesbyid/',GetcmpyrolesbycmpyId.as_view()),
    path('voffice/admin/sendmail/', ResendEmail.as_view()),

    #department
    path('voffice/admin/getdepartments', GetDepartments.as_view()),
    path('voffice/admin/addupddepartment', AddDepartments.as_view()),

    #branch
    path('voffice/admin/getbranch', GetBranch.as_view()),
    path('voffice/admin/addbranch', AddBranch.as_view()),

    #subcompanies
    path('voffice/admin/getsubcompanies', GetSubCompanies.as_view()),
    path('voffice/admin/addsubcompanies', ADDSubCompanies.as_view()),
]
